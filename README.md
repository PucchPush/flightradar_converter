# Flightradar converter

This program parses and extracts information from FlightRadar24 flight CSV files.

The function `parse_flightradar_file` accepts a path to the flight CSV file and returns a list of timestamped TrackReport instances containing position in lat/long, altitude, ground speed and heading.

# Usage
The program can be used both as an import and in stand-alone. The purpose of stand-alone is mainly to troubleshoot and verify parsing. The main intended use is as an import.
## As an import
```python
import flighradar_converter.csv_converter as frc
track_list, parsed_track_reports = frc.parse_flightradar_file("<Path here>")

track_list[0].latitude
track_list[0].longitude
...
```

## Stand-alone
```bash
python csv_converter.py -f <CSV file path>
```
