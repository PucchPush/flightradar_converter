import csv
import argparse
import pathlib
import math

class TrackReport:
  
  def __init__(self, position, altitude, direction, speed, timestamp, callsign):
    """
      This class represents one track report from a flightradar24 csv track file.

      Params:
        position - String containing the latitude and longitude on the format "<LAT>,<LONG>" in degrees (DD)
        altitude - Int/Float representing the altitude of the flight in feet
        direction - Int/Float representing the heading of the aircraft in degrees
        speed - Int/Float representing the ground speed of the aircraft in knots
        timestamp - Int representing seconds since january 1st 1970 (Unix Epoch time) in Zulu time (UTC)
        callsign - String representing the callsign of the flight
    """

    self.latitude = None
    self.longitude = None
    self.parse_position(position)
    self.altitude = float(altitude)
    self.direction = float(direction)
    self.speed = float(speed)
    self.track_timestamp = timestamp
    self.callsign = callsign
 

  def parse_position(self, position):
    """
      This function converts a composite lat/long string into two floats representing lat and long respectively.
      It modifies the self.latitude and self.longitude of this object with the values from position.

      Params:
        position - String containing the latitude and longitude on the format "<LAT>,<LONG>" in degrees

      Returns:
        Void
    """

    lat_long = position.split(",")
    assert len(lat_long) == 2

    self.latitude = float(lat_long[0])
    self.longitude = float(lat_long[1])


  def get_latitude_rad(self):
    """
      Returns latitude in radians
    """
    return self.latitude * (math.pi/180)


  def get_latitude_deg(self):
    """
      Returns latitude in degrees (DD)
    """
    return self.latitude


  def get_longitude_rad(self):
    """
      Returns longitude in radians
    """
    return self.longitude * (math.pi/180)


  def get_longitude_deg(self):
    """
      Returns longitude in degrees (DD)
    """
    return self.longitude


  def get_altitude_meters(self):
    """
      Returns altitude in meters
    """
    return float(self.altitude) * 0.3048


  def get_altitude_feet(self):
    """
      Returns altitude in feet
    """
    return self.altitude

  def get_speed_mps(self):
    """
      Returns speed in meters per second
    """
    return float(self.speed) * 0.5144444


  def get_speed_knots(self):
    """
      Returns speed in knots
    """
    return self.speed


def parse_flightradar_file(flightradar_track_file):
  """
    This function parses a flightradar24 csv track file for a single flight.
    It produces a list of TrackReport instances, one for each row in the csv track file.

    Params:
      flightradar_track_file - String containing a path to the csv track file to parse

    Returns:
      track_list - A list of TrackReport instances representing the individual track reports from the csv track file
      track_reports_parsed - Int representing the number of lines input file that were parsed
  """

  track_list = list()
  track_reports_parsed = 0

  with open(flightradar_track_file) as track_file:
    track_reader = csv.DictReader(track_file, delimiter=",")
    
    # Read csv file line by line
    for track_report_dict in track_reader:

      # Create a track report for each line
      track_report = TrackReport(track_report_dict["Position"],
                                 track_report_dict["Altitude"],
                                 track_report_dict["Direction"],
                                 track_report_dict["Speed"],
                                 track_report_dict["Timestamp"],
                                 track_report_dict["Callsign"])
      
      track_list.append(track_report)
      track_reports_parsed += 1

  return track_list, track_reports_parsed


def parse_arguments():
  """
    Setup function for the argumentparser used if running this program stand-alone

    Params:
      Void

    Returns:
      args - Object containing ArgumentParser arguments and their values
  """

  parser = argparse.ArgumentParser()
  parser.add_argument("-f", dest="flightradar_track_file", action="store", required=True, help="Flightradar track csv file", type=str)

  args = parser.parse_args()

  return args


def main():
  """
    Main function used if running this program stand-alone
  """

  args = parse_arguments()
  
  # Make sure the track file exists
  track_file_path = pathlib.Path(args.flightradar_track_file)
  
  # If it does, parse it
  if track_file_path.is_file():
    track_reports, track_reports_parsed = parse_flightradar_file(args.flightradar_track_file)
  
  # Else abort
  else:
    print(f"file not found: {args.flightradar_track_file}")
    print("aborting")
    return

  assert len(track_reports) > 0
  print(f"Parsed {args.flightradar_track_file}")
  print(f"Parsed reports: {track_reports_parsed}")
  print(f"Created TrackReports: {len(track_reports)}")

  # Count the total lines in the input file
  with open(args.flightradar_track_file, "r") as f:
    file_lines = len(f.readlines())

  print(f"Input file total lines: {file_lines}")

if __name__ == "__main__":
  main()
